import math


vibor = int(input('Выберите функцию.(1-G), (2-F),(3-Y): '))
a = float(input('Введите a:'))
x = float(input('Введите x:'))

if vibor == 1:
    G = (5*(9*a**2 - 26*a*x + 16*x**2)) / (20*a**2 + 29*a*x + 5*x**2)
    if (20*a**2 + 29*a*x + 5*x**2) == 0:
        print('Невозможно вычислить'), exit()
    print(f'G = {G:.5}')

elif vibor == 2:
    try:
        F = 1 / (math.cos(14 * a ** 2 - 19 * a * x + 6 * x ** 2))
    except ValueError or ZeroDivisionError:
        print('Невозможно вычислить')
    print(f'F = {F:.5}')
elif vibor == 3:
    if -80*a**2 + 6*a*x + 35*x**2 + 1 < 0:
        print('Невозможно вычислить'), exit()
    Y = (math.log(-80*a**2 + 6*a*x + 35*x**2 + 1) / (math.log(10)))
    print(f'Y = {Y:.5}')
else:
    print('Ошибка: выберите номер функции от 1 до 3'), quit()
