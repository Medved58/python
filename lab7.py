import math


data = list(map(float, input('Введите через пробел а, xmin, хmax, шаг: ').split()))
a = int(data[0])
xmin = int(data[1])
xmax = int(data[2])
shag = int(data[3])
g_list = []
f_list = []
y_list = []
count = 0
for x in range(xmin, xmax + 1, shag):
    try:
        G = (5 * (9 * a ** 2 - 26 * a * x + 16 * x ** 2)) / (20 * a ** 2 + 29 * a * x + 5 * x ** 2)
        F = 1 / (math.cos(14 * a ** 2 - 19 * a * x + 6 * x ** 2))
        Y = (math.log(-80 * a ** 2 + 6 * a * x + 35 * x ** 2 + 1) / (math.log(10)))
        g_list.append(G)
        f_list.append(F)
        y_list.append(Y)
    except ValueError:
        G = None
        F = None
        Y = None
        g_list.append(G)
        f_list.append(F)
        y_list.append(Y)

file = open('results.txt', 'w')
file.write(f"{g_list}\n{f_list}\n{y_list}")
file.close()
data = []
file = open('results.txt', 'r')
[data.append(line.split()) for line in file]
file.close()
for x in range(len(data[0])):
    print(f"G: f(x) = {data[0][x]}")
for x in range(len(data[1])):
    print(f"F: f(x) = {data[1][x]}")
for x in range(len(data[2])):
    print(f"Y: f(x) = {data[2][x]}")

