import math


a = float(input('Введите a:'))
x = float(input('Введите x:'))
G = (5*(9*a**2 - 26*a*x + 16*x**2)) / (20*a**2 + 29*a*x + 5*x**2)
print(f'G = {G:.5}')

a = float(input('Введите a:'))
x = float(input('Введите x:'))
F = 1 / (math.cos(14*a**2 - 19*a*x + 6*x**2))
print(f'F = {F:.5}')

a = float(input('Введите a:'))
x = float(input('Введите x:'))
Y = (math.log(-80*a**2 + 6*a*x + 35*x**2 + 1) / (math.log(10)))
print(f'Y = {Y:.5}')
