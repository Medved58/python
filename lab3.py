import math


vibor = int(input('Выберите функцию.(1-G), (2-F),(3-Y): '))
xmin = int(input('Введите минимальную границу x: '))
xmax = int(input('Введите максимальную границу x: '))
shag = int(input('Введите шаг измения значения x: '))
a = float(input('Введите a: '))

if vibor == 1:
    for x in range(xmin, xmax + 1, shag):
        try:
            G = (5 * (9 * a ** 2 - 26 * a * x + 16 * x ** 2)) / (20 * a ** 2 + 29 * a * x + 5 * x ** 2)
        except ValueError or ZeroDivisionError:
            print('Невозможно вычислить'), exit()
        print('x =', x, ', ' f'G = {G:.5}')

elif vibor == 2:
    for x in range(xmin, xmax + 1, shag):
        try:
            F = 1 / (math.cos(14 * a ** 2 - 19 * a * x + 6 * x ** 2))
        except ValueError or ZeroDivisionError:
            print('Невозможно вычислить'), exit()
        print('x =', x, ', ' f'F = {F:.5}')

elif vibor == 3:
    for x in range(xmin, xmax + 1, shag):
        try:
            Y = (math.log(-80 * a ** 2 + 6 * a * x + 35 * x ** 2 + 1) / (math.log(10)))
        except ValueError:
            print('Невозможно вычислить'), exit()
        print('x =', x, ', ' f'Y = {Y:.5}')
else:
    print('Ошибка: выберите номер функции от 1 до 3'), quit()
