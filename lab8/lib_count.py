def in_area(dot, center, rad):
    return pow((center[0]-dot[0]), 2) + pow((center[1]-dot[1]), 2) <= rad**2


def search(points, center, rad):
    count = 0
    for point in points:
        if in_area(point, center, rad):
            count += 1
    return count

