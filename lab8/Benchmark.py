import time
import lib_count
import random
import matplotlib.pyplot as plt

num_data = []
time_data = []
all_time = 0
for n in range(int(10e4), int(10e5), int(10e4)):
    for _ in range(3):
        coordinates = [(random.uniform(-50, 50), random.uniform(-50, 50)) for point in range(n)]
        center = (random.uniform(-50, 50), random.uniform(-50, 50))
        radius = random.uniform(-50, 50)
        start = time.time()
        count = lib_count.search(coordinates, center, radius)
        stop = time.time()
        all_time += (stop - start)
    coordinates = []
    time_data.append(all_time/3)
    num_data.append(n)
    print(all_time/3)

    file = open("program_time", 'a')
    file.write(f'{(all_time / 3):.1f}\n')
    file.close()
    all_time = 0

plt.plot(num_data, time_data, 'o')
plt.plot(num_data, time_data, 'b', c='red')
plt.xlabel('кол-во точек')
plt.ylabel('время выполнения, с.')
plt.title('График зависимости кол-ва точек от времени')

plt.show()


