import math


def program():
    data = list(map(float, input('Введите через пробел а, xmin, хmax, шаг: ').split()))
    a = int(data[0])
    xmin = int(data[1])
    xmax = int(data[2])
    shag = int(data[3])
    g_list = []
    f_list = []
    y_list = []
    count = 0
    for x in range(xmin, xmax + 1, shag):
        try:
            G = (5 * (9 * a ** 2 - 26 * a * x + 16 * x ** 2)) / (20 * a ** 2 + 29 * a * x + 5 * x ** 2)
            F = 1 / (math.cos(14 * a ** 2 - 19 * a * x + 6 * x ** 2))
            Y = (math.log(-80 * a ** 2 + 6 * a * x + 35 * x ** 2 + 1) / (math.log(10)))
            g_list.append(G)
            f_list.append(F)
            y_list.append(Y)
        except ValueError:
            G = None
            F = None
            Y = None
            g_list.append(G)
            f_list.append(F)
            y_list.append(Y)
    for g_list, f_list, y_list in zip(g_list, f_list, y_list):
        print(f'{g_list}\t{f_list}\t{y_list}')
while True or OverflowError:
    program()
    again = input("Хотите начать программу сначала? [Y/N]: ")
    if again not in ['Y', 'y']:
        break
